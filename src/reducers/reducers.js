import * as actions from '../actions/actions'

export default function reducer(state, action) {
    switch(action.type) {
        case actions.onclick:
            let x = state.clicks;
            let newState = Object.assign({}, {
            clicks:++x
            });

            return newState;

        default:
            return state;
    }
}