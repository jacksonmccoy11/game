import React, { Component } from 'react';
import PropTypes from 'prop-types'

class Game extends Component {

    render() {
        return (
            <div>
                <span>
                    Clicks: {this.props.clicks}
                </span>
                <button onClick={this.props.onClick}>Click Me</button>
            </div>
        )
    }
}

Game.propTypes = {
    clicks: PropTypes.number.isRequired,
    onClick: PropTypes.func.isRequired
};

export default Game;