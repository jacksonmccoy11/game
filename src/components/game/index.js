import { connect } from 'react-redux';
import { doClick } from '../../actions/actions';
import Game from './game';

const mapStateToProps = state => {
    return {
        clicks: state.clicks,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onClick: () => {
            dispatch(doClick());
        }
    }
};

const GameContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Game);

export default GameContainer