import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import digApp from './reducers/reducers'
import App from './components/app/App'
import './index.css'

const initialState = {
    clicks: 0
};

let store = createStore(digApp, initialState);

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);